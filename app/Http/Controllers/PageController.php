<?php

namespace App\Http\Controllers;


use App\Models\Page;
use Illuminate\Http\Request;
use InvalidArgumentException;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    function index(Request $req){

        $collection = Page::orderBy('id','desc')->get();
        
        $data = [
            'collection' => $collection,
            
        ];

        return view('page.index', $data);
    }

    function edit($id){
        $find = Page::find($id);

        if (!$find) { //jika ada data $find tidak ditemukan maka hasil akan not found
            return abort(404);
        }

        return view('page.edit', [
            'item' => $find
        ]);
    }

    public function upload($img, $folder, $default = null)
    {
        try {
            if ($default) {
                return $default;
            }

            $folder_ext = explode('/', $folder);
            $path = 'storage/upload/'.$folder;
            if (!file_exists($path)) {
                mkdir($path, 0755, true);
            }
            $extension = $img->getClientOriginalExtension();
            $file = rand(000000, 999999).'_'.$folder_ext[0].'.'.$extension;
            $path = $img->move('storage/upload/'.$folder, $file);
            return 'storage/upload/'.$folder.'/'.$file;
        } catch (\Throwable $th) {
            throw new InvalidArgumentException($th->getMessage(), 500);
        }
    }

    function deleteFile($img) {
        if(file_exists($img)){
          unlink($img);
        }
    }

    function store(Request $req){
        try {
            $validator = Validator::make($req->all(), [
                'title' => 'required',
                'description' => 'required',
                'image' => 'required|image|size:2222'
            ]);
            if ($validator->fails()) {
                throw new InvalidArgumentException($validator->errors()->first(), 422);
            }

            Page::create(array_merge(
                $req->only(['title', 'description']), 
                [
                    'image' => $this->upload($req->image, 'page')
                ]
            ));
            
            return redirect('/pages');
        } catch (\Throwable $th) {
            return response()->json([
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage(),
                'line' => $th->getLine(),
                'file' => $th->getFile()
            ], 500);
        }
    }
    
    function update(Request $req){
        try {
            $validator = Validator::make($req->all(), [
                'id' => 'required',
                'title' => 'required',
                'description' => 'required',
                'image' => 'image|size:2222'
            ]);
            if ($validator->fails()) {
                throw new InvalidArgumentException($validator->errors()->first(), 422);
            }

            $find = Page::find($req->id);

            $imageOld = $find->image;

            if (!$find) {
                return abort(404);
            }

            $find->update(array_merge(
                $req->only(['title', 'description']),
                [
                    'image' => $this->upload($req->image, 'page', $find->image)
                ]
            ));

            if ($req->image) {
                $this->deleteFile($imageOld);
            }
            
            return redirect('/pages');
        } catch (\Throwable $th) {
            return response()->json([
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage(),
                'line' => $th->getLine(),
                'file' => $th->getFile()
            ], 500);
        }
    }

    function delete(Request $req){
        try {
            $find = Page::find($req->id);

            if (!$find) { return abort(404); }

            $imageOld = $find->image;

            $find->delete();

            $this->deleteFile($imageOld);

            return redirect('/pages');
        } catch (\Throwable $th) {
            return response()->json([
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage(),
                'line' => $th->getLine(),
                'file' => $th->getFile()
            ], 500);
        }
    }
}
