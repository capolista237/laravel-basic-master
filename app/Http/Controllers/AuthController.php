<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use InvalidArgumentException;

class AuthController extends Controller
{
    function index(Request $req){
        try {
            /* untuk mencari data USER berdasarkan EMAIL */
            $find = User::where('email', $req->email)->first();

            /**
             * untuk mengecek apa USER ditemukan atau tidak 
             * jika tidak ditemukan akan muncul notifikasi dibawah
             */
            if(!$find) {
                throw new InvalidArgumentException('Akun tidak ditemukan', 500);
            }

        } catch (\Throwable $th) {
            return response()->json([
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
}