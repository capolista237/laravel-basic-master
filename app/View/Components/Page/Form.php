<?php

namespace App\View\Components\Page;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use App\Models\Category;

class Form extends Component
{
    /**
     * Create a new component instance.
     */
    public $item;
    public function __construct($item = null)
    {
       $this->item = $item;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        $categories = Category::orderBy('title','asc')->get();
        return view('components.page.form', [
            'categories' => $categories
        ]);
    }
}
