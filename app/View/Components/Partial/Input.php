<?php

namespace App\View\Components\Partial;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Input extends Component
{

  public $label;
  public $type;
  public $name;
  public $id;
  public $placeholder;
  public $value;
  public $offrequired;

  public function __construct($label, $name, $id, $type = 'text', $placeholder = '...', $value = null, $offrequired = null)
  {
    $this->label = $label;
    $this->name = $name;
    $this->id = $id;
    $this->type = $type;
    $this->placeholder = $placeholder;
    $this->value = $value;
    $this->offrequired = $offrequired;
  }

  public function render(): View|Closure|string
  {
    return view('components.partial.input');
  }
}
