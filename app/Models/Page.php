<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $table = 'pages';
    
    protected $fillable = ['title', 'id_category', 'description', 'image'];
    
    public function scopeWithParameters($query, $req) {
        return $query = $query;
    }

    public function category() {
        return $this->belongsTo(Category::class, 'id_category');
    }

}
