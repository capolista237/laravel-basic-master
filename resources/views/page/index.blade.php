<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Artikel</title>

  <link 
    rel="stylesheet" type="text/css" 
    href="{{asset('custom/custom.css?used=2607')}}" />
</head>
<body>

  <x-page.form />

  <h4>Daftar Artikel :</h4>

  <table>
    <thead>
      <tr>
        <td>No</td>
        <td>Judul</td>
        <td>Gambar</td>
        <td>Singkat</td>
        <td>Aksi</td>
      </tr>
    </thead>
    <tbody>
      @foreach ($collection as $number => $item)
        <tr>
          <td>{{$number + 1}}</td>
          <td>{{$item->title}}</td>
          <td>
            <img 
              src="{{asset($item->image)}}"
              alt="image" style="width: 200px" />
          </td>
          <td>
            {{Str::words($item->description)}}
          </td>
          <td></td>
        </tr>
      @endforeach
    </tbody>
  </table>
  
</body>
</html>