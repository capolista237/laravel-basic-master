<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Edit Daftar Pengguna</title>

  <link 
    rel="stylesheet" type="text/css" 
    href="{{asset('custom/custom.css?ls=25070857')}}" />
</head>
<body>

  <form action="{{url('user/update')}}" method="post">
    @csrf
    <input type="hidden" name="id" value="{{$item->id}}" />
    <x-partial.input
      label="Nama Depan" placeholder="Masukan nama depan"
      id="firstNameInput" name="first_name" 
      value="{{$item->first_name}}" />
    <x-partial.input
      label="Nama Belakang" placeholder="Masukan nama belakang"
      id="lastNameInput" name="last_name" 
      value="{{$item->last_name}}" />
    <x-partial.input
      label="Tanggal Lahir" type="date" 
      id="birthDateInput" name="birth_date" 
      value="{{$item->birth_date}}" />
    <x-partial.input
      label="Email" placeholder="Masukan email" 
      id="emailInput" name="email"
      value="{{$item->email}}" />
    <x-partial.input
      label="Password" placeholder="(jika ingin melakukan perubahan)"
      id="passwordInput" name="password"
      offrequired />

    <a href="{{url('/user')}}">Kembali</a>
      
    <button type="submit">Simpan</button>

  </form>

</body>
</html>