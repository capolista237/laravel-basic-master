<div class="mb-10px">
  <label for="{{$id}}">{{$label}} :</label>
  <input 
    type="{{$type}}" name="{{$name}}" 
    id="{{$id}}" 
    placeholder="{{$placeholder}}" 
    value="{{$value}}"
    @if(!$offrequired) required @endif />
</div>