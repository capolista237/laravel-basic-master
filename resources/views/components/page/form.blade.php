<form
    class="card" method="post"
    action="{{url($action ?? 'pages/store')}}" 
    enctype="multipart/form-data">

    @csrf

    <input type="hidden" name="id" value="{{$item?->id ?? ''}}" />

    <x-partial.input
      label="Judul" placeholder="Masukan judul"
      id="titleInput" name="title" 
      value="{{$item?->title ?? ''}}" />

      <div class="mb-10px">
        <label for="categorySelect">Kategori</label>
        <select name="id_category" id="categorySelect" required>
          <option value="">-Pilih Kategori -</option>
          @foreach ($categories as $category)
          <option value="{{$category->id}}"
            @if(($item?->id_category ?? '') == $category->id) selected
            @endif
            >{{$category->title}}</option>
          @endforeach
</select>
</div>

    <div class="mb-10px">
      <label for="descriptionInput">Deskripsi</label>
      <textarea 
        name="description" id="descriptionInput" 
        cols="10" rows="3"
        placeholder="Masukan deskripsi">{{$item?->description ?? ''}}</textarea>
    </div>

    <div class="mb-10px">
      <label for="imageInput">Gambar</label>
      <input 
        type="file" accept=".jpg, .png, .webp, .jpeg"
        name="image" id="imageInput" required />
        @if (($item?->image ?? ''))
        <div>
          <img
          src="{{asset($item->image)}}
          alt="image" style="width: 100px" />
          </div>
          @endif
    </div>

    <button type="submit">Simpan</button>
</form>