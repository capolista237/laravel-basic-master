<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Login</title>

  <link 
    rel="stylesheet" type="text/css" 
    href="{{asset('custom/custom.css?used=08082023-0824')}}" />
</head>
<body>
    <div class="card box-shadow radius-10px">

  <form action="{{url('authentication')}}" method="post">
    @csrf

    <div class="mb-10px">
      <label for="emailInput">Email</label>
      <input type="email" name="email" placeholder="Masukan Email" required />
    </div>

    <div class="mb-10px">
      <label for="passwordInput">Password</label>
      <input type="password" name="password" placeholder="Masukan password" required />
    </div>

    <button type="submit">Masuk</button>
  </form>
</div>
</body>
</html>