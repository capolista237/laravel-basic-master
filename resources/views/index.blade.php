<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Daftar Fitur</title>
</head>
<body>
  <h4>Daftar Fitur</h4>
  <ul>
    <li>
      <a href="/user">Daftar Pengguna</a>
    </li>
    <li>
      <a href="/category">Kategori</a>
    </li>
    <li>
      <a href="/pages">Artikel</a>
    </li>
    <li>
      <a href="/logout">Logout</a>
    </li>
  </ul>
</body>
</html>